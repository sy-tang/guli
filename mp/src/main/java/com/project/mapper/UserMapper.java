package com.project.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.project.entity.User;
import org.springframework.stereotype.Repository;

/**
 * @author 汤小白
 * @date 2020/7/17
 */
@Repository
public interface UserMapper extends BaseMapper<User> {
}
