package com.project.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

/**
 * @author 汤小白
 * @date 2020/7/17
 */
@Data
public class User {
    private Long id;
    private String name;
    private String email;
    private Integer age;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
    @Version
    @TableField(fill = FieldFill.INSERT)
    private Integer version;

    @TableLogic  // 逻辑删除
    @TableField(fill = FieldFill.INSERT)
    private boolean isDelete;
}
