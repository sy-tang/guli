package com.project.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.project.entity.User;
import com.project.mapper.UserMapper;
import com.project.utils.Result;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 汤小白
 * @date 2020/7/17
 */
@RestController
@RequestMapping("/hello")
public class HelloController {
    @Autowired
    UserMapper userMapper;

    @ApiOperation("分页查询")
    @GetMapping("page/{currt}/{size}")
    public Result pageSelect(
            @PathVariable("currt")
            @ApiParam(name = "currt",value = "当前页码",required = true)
            Integer currt,
            @PathVariable("size")
            @ApiParam(name = "size",value = "每页大小",required = true)
            Integer size){
        Page<User> page = new Page<>(currt,size);
        userMapper.selectPage(page,null);
        return Result.ok().data("total",page.getTotal()).data("items",page.getRecords());
    }
}
