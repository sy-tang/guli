package com.project.utils;

import com.baomidou.mybatisplus.extension.api.R;
import com.sun.org.apache.xpath.internal.operations.Bool;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 汤小白
 * @date 2020/7/17
 */
@Data
public class Result {
    private Integer code;  // 状态码
    private Boolean success; //
    private String message; // 执行信息
    private Map<String,Object> data = new HashMap<>();

    private Result() {
    }

    public Result message(String message){
        this.setMessage(message);
        return this;
    }
    public Result code (Integer code){
        this.setCode(code);
        return this;
    }
    public Result success(Boolean success){
        this.setSuccess(success);
        return this;
    }

    public static Result ok(){
        Result result = new Result();
        result.setMessage("执行成功");
        result.setCode(ResultCode.SUCCESS);
        result.setSuccess(true);
        return result;
    }
    public static Result error(){
        Result result = new Result();
        result.setMessage("执行失败");
        result.setCode(ResultCode.ERROR);
        result.setSuccess(false);
        return result;
    }

    public Result data(String key,Object value){
        this.data.put(key,value);
        return this;
    }
    public Result data(Map map){
        this.setData(map);
        return this;
    }

}
