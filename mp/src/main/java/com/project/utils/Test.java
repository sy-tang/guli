package com.project.utils;

import javax.xml.soap.Text;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author 汤小白
 * @date 2020/7/20
 */
public class Test {
    public static volatile AtomicLong f = new AtomicLong(){
        @Override
        public int intValue() {
                return 0;
        }
    };

    //public void hello(){
    //    synchronized (this){
    //        System.out.println("hello");
    //    }
    //}
    public static  void test(){
        f.getAndIncrement();
    }
    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(10000);
        for (int i = 0; i < 10000; i++) {
            executor.execute(()->{Test.test();});
        }
        executor.shutdown();
        System.out.println(Thread.currentThread().getName()+"--f:"+f);
    }
}
