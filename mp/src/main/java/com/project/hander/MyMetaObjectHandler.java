package com.project.hander;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;


import java.time.LocalDate;
import java.util.Date;

/**
 * @author 汤小白
 * @date 2020/7/17
 * 自动填充
 */
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {
    /**
     * 插入时自动填充
     * @param metaObject
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        this.fillStrategy(metaObject,"createTime",new Date());
        this.fillStrategy(metaObject,"updateTime",new Date());
        this.fillStrategy(metaObject,"version",1);
        this.fillStrategy(metaObject,"isDelete",0);
    }


    /**
     * 修改时自动填充
     * @param metaObject
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        this.fillStrategy(metaObject,"updateTime",new Date());
    }

}
