package com.project;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.project.entity.User;
import com.project.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@SpringBootTest
class MpApplicationTests {

    @Autowired
    UserMapper userMapper;

    @Test
    void contextLoads() {
    }

    /**
     * 测试查询
     */
    @Test
    public void getUser(){
        User user = userMapper.selectById(1);
        System.out.println(user); //result:User(id=1, name=Jone, email=test1@baomidou.com, age=18)
    }

    /**
     * 测试添加
     */
    @Test
    public void addUser(){
        User user = new User();
        user.setAge(23);
        user.setName("tangtamg");
        user.setEmail("153513166@qq.com");
        userMapper.insert(user);
        System.out.println(user);
    }

    /**
     * 使用乐观锁，进行修改时，需要先查询数据，在进行修改数据
     */
    @Test
    public void updUser(){
        User user = userMapper.selectById(1);  // 先查询
        user.setAge(25);
        userMapper.updateById(user);  // 在修改
    }

    /**
     * 常用查询方式
     */
    @Test
    public void select(){
        //User user = userMapper.selectById(1);
        //System.out.println(user);
        //List<User> users = userMapper.selectBatchIds(Arrays.asList(1, 3, 4));
        //System.out.println(users);
        //HashMap<String, Object> map = new HashMap<>();
        //map.put("age",20);
        //map.put("name","tang");
        //List<User> users = userMapper.selectByMap(map);
        //System.out.println(users);

        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("age",20);
        List<User> users = userMapper.selectList(wrapper);
        System.out.println(users);


    }


    /**
     * 删除
     */
    @Test
    public void delUser(){
        int i = userMapper.deleteById(6);
        System.out.println(i);
        //userMapper.deleteBatchIds(Arrays.asList(1, 3));  // 批量删除
    }


    /**
     * 分页查询
     */
    @Test
    public void pageSelect(){
        Page<User> page = new Page<>(1, 2);
        Page<User> result = userMapper.selectPage(page, null);
        System.out.println(result.getSize()); // 每页大小
        System.out.println(result.getTotal());  //数据库中总记录数
        System.out.println(result.getRecords());  // 分页数据
        System.out.println(result.hasNext());   // 是否有前一页
        System.out.println(result.hasPrevious()); // 是否有后一页
        System.out.println(result.getPages());  // 一共有多少页

    }


    /**
     * 逻辑删除 ,添加逻辑删除注解@TableLogic,配置自动填充即可。之前的版本可能还要配置逻辑删除插件
     */
    @Test
    public void delete(){
        int i = userMapper.deleteById(1283957471714525185L);
        System.out.println(i);
    }

    /**
     * 条件构造
     */
    public void condition(){
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("age",20);  //eq等于，ne不等于
        userMapper.selectList(wrapper);


        System.out.println("==============================");
        wrapper.gt("age",20);  // gt大于  ge大于等于
       userMapper.selectList(wrapper);

        System.out.println("==============================");

        wrapper.lt("age",20); // lt 小于，le小于等于
        userMapper.selectList(wrapper);

        System.out.println("===============================");
        wrapper.between("age",20,23); // between  notbetween
        userMapper.selectList(wrapper);

        System.out.println("===============================");
        wrapper.last("limit 4");
        userMapper.selectList(wrapper);


    }
}
