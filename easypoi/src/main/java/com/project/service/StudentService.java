package com.project.service;

import com.project.pojo.Student;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 汤小白
 * @since 2020-08-09
 */
public interface StudentService extends IService<Student> {

}
