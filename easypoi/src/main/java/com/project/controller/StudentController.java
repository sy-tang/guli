package com.project.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 汤小白
 * @since 2020-08-09
 */
@RestController
@RequestMapping("/student")
public class StudentController {

}

