package com.guigu.service.impl;

import com.guigu.pojo.Role;
import com.guigu.mapper.RoleMapper;
import com.guigu.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 汤小白
 * @since 2020-08-09
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

}
