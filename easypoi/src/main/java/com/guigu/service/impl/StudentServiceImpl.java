package com.guigu.service.impl;

import com.guigu.pojo.Student;
import com.guigu.mapper.StudentMapper;
import com.guigu.service.StudentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 汤小白
 * @since 2020-08-09
 */
@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements StudentService {

}
