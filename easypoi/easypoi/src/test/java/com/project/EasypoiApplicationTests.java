package com.project;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ExcelBaseParams;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import com.alibaba.fastjson.JSON;
import com.project.entity.User;
import net.minidev.json.JSONUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

@SpringBootTest
class EasypoiApplicationTests {

    @Test
    void contextLoads() {
    }


    @Test
    public void test(){
        try {
            FileInputStream file = new FileInputStream("D:\\迅雷下载\\test.xlsx"); // 获取文件流
            ImportParams params = new ImportParams();

            params.setHeadRows(2);  // 表头行数
            List<User> user = ExcelImportUtil.importExcel(file, User.class, params);  // 导入excel

//            ExportParams exportParams = new ExportParams();
//
//            ExcelExportUtil.exportExcel(exportParams,User.class,user    );
            System.out.println(user.size());
            System.out.println(JSON.toJSONString(user));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
