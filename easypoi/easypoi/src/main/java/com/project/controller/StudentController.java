package com.project.controller;


import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.project.entity.Role;
import com.project.entity.User;
import com.project.pojo.Student;
import com.project.service.RoleService;
import com.project.service.StudentService;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 汤小白
 * @since 2020-08-09
 */
@RestController
@RequestMapping("/student")
public class StudentController {

    @Autowired
    RoleService roleService;
    @Autowired
    StudentService studentService;

    //文件的导入，存储在数据库中
    //@PostMapping("/file")
    //public List<User> test(@RequestParam MultipartFile file){
    //
    //    //File file = new File("C:\\Users\\汤\\Desktop\\test1.xlsx");
    //    try {
    //        //FileInputStream inputStream = new FileInputStream(file);
    //        InputStream inputStream = file.getInputStream();
    //        ImportParams params = new ImportParams();
    //        params.setHeadRows(2);
    //        params.setNeedVerfiy(true);  //开启校验
    //        ExcelImportResult<User> result = ExcelImportUtil.importExcelMore(inputStream, User.class, params);
    //        if (result.getFailList().size()>0){
    //            return null;
    //        }
    //        for (User user : result.getList()){
    //            List<Role> roleList = user.getRole();
    //            if (roleList.size()>0){
    //                Role role = roleList.get(0);
    //                com.project.pojo.Role role1 = new com.project.pojo.Role();
    //                BeanUtils.copyProperties(role,role1);
    //                //DOTO  判断role在数据库中是否存在
    //                roleService.save(role1);
    //                int role_id = role1.getId();
    //                Student student = new Student();
    //                student.setRoleId(role_id);
    //                BeanUtils.copyProperties(user,student);
    //                studentService.save(student);
    //            }
    //        }
    //        return result.getList();
    //    } catch (Exception e) {
    //        e.printStackTrace();
    //    }
    //    return null;
    //}

    @GetMapping("/down")
    public String tt(HttpServletResponse servletResponse) throws IOException {
        List<Student> list = studentService.list(null);
        if (list.size()<1){
            return null;
        }
        ArrayList<User> users = new ArrayList<>();

        for (Student student:list) {
            Role role = new Role();
            User user = new User();
            //List roleList = new ArrayList<Role>();
            com.project.pojo.Role role1= roleService.getById(student.getRoleId());
            BeanUtils.copyProperties(student,user);
            BeanUtils.copyProperties(role1,role);
            //roleList.add(role);
            user.setRole(role);
            users.add(user);
        }
        System.out.println("---------------");
        System.out.println(users);
        String name = "数据表";  // 文件的名称
        servletResponse.setHeader("content-Type", "application/vnd.ms-excel");
        servletResponse.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(name + ".xlsx", "utf-8"));
        servletResponse.setHeader("Access-Control-Expose-Headers", "ContentDisposition");
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("学生表","学生"),User.class,users);
        workbook.write(outputStream);
        servletResponse.setHeader("Content-Length", String.valueOf(outputStream.size()));
        try {
            ServletOutputStream out = servletResponse.getOutputStream();
            out.write(outputStream.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            workbook.close();
        }
        return "success";
    }

}

