package com.project.mapper;

import com.project.pojo.Student;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 汤小白
 * @since 2020-08-09
 */
public interface StudentMapper extends BaseMapper<Student> {

}
