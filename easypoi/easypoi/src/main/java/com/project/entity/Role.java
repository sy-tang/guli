package com.project.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Date;

//import static com.project.entity.User.DATE_REGEXP;

@Data
public class Role implements Serializable {

    @Excel(name = "职位名称",orderNum = "5")
    @NotBlank(message = "职位名称不能为空")
    private String roleName;

    @Excel(name = "担任时间",databaseFormat = "yyyyMMddHHmmss",format = "yyyy-MM-dd HH-mm-ss",orderNum = "6")
   // @Pattern(regexp = DATE_REGEXP,message = "时间格式错误")
    private Date createTime;
}
