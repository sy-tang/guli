package com.project.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelCollection;
import cn.afterturn.easypoi.excel.annotation.ExcelEntity;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import cn.afterturn.easypoi.handler.inter.IExcelDataModel;
import cn.afterturn.easypoi.handler.inter.IExcelModel;
import lombok.Data;
import org.apache.ibatis.annotations.Param;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.List;

/**
 * excelPOi基础校验  是否为空，格式，时间格式等
 */
@Data
public class User  implements IExcelDataModel, IExcelModel , Serializable {

    private int rowNum;  // 接收错误的行号
    private String errorMsg;  // 接收错误的信息

    // 时间格式校验正则
    //public static final String DATE_REGEXP = "(Mon|Tue|Wed|Thu|Fri|Sat|Sun)( )(Dec|Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov)( )\\d{2}( )(00:00:00)( )(CST)( )\\d{4}";



    @Excel(name = "姓名",orderNum = "1")
    @NotBlank(message = "[姓名]不能为空")
    private String name;


    @Excel(name = "年龄",orderNum = "2")
    @NotBlank(message = "[年龄]不能为空")
    private String age;   // 使用了校验的地方，数据类型只能是String了

    @Excel(name = "学校",orderNum = "3")
    private String school;

    @Excel(name = "性别", replace = {"男_1","女_0"}, suffix = "生",orderNum = "4")
    @Pattern(regexp="[01]", message = "性别错误")
    private String sex;

    @ExcelEntity(name="职位",show = true)
    private Role role;




}
