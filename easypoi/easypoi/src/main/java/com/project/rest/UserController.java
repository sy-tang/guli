package com.project.rest;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import cn.afterturn.easypoi.util.PoiValidationUtil;
import com.project.entity.Role;
import com.project.entity.User;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

@RestController
@RequestMapping("/user")
public class UserController {

    @GetMapping("file")
    public List uploadExcel(){
        File file1 = new File("C:\\Users\\汤\\Desktop\\test1.xlsx");
        if (file1.length()<=1){
            System.out.println("文件为空");
            return null;
        }
        try {
            // 获取文件
            FileInputStream file = new FileInputStream(file1); // 获取文件流
            //设置表头行数
            ImportParams params = new ImportParams();
            params.setHeadRows(2);
            params.setNeedVerfiy(true);  //开启校验
            ExcelImportResult<User> result = ExcelImportUtil.importExcelMore(file, User.class, params);
            if (result.isVerfiyFail()){  // 判断是否通过校验
                ArrayList arrayList = new ArrayList();
                arrayList.add(result.isVerfiyFail());
                arrayList.addAll(result.getFailList());  // 获取所有未通过检查的记录
                for (User entity : result.getFailList()) {
                    System.out.println("第"+entity.getRowNum()+"行"+"出现的错误是："+entity.getErrorMsg());
                }
                return arrayList;
            }
            List<User> list = result.getList();  // 获取所有通过的记录
//            // 读取excel表
//            List<User> list = ExcelImportUtil.importExcel(file, User.class, params);
//
//            System.out.println("----"+result.isVerfiyFail());


            //校验集合中的字段是否符合
            //1.先将所有的记录整合到一起
            ArrayList<User> users = new ArrayList<>();
            users.addAll(result.getFailList());
            users.addAll(result.getList());
            //遍历
            for (User u:users) {
                StringJoiner joiner = new StringJoiner(",");
                joiner.add(u.getErrorMsg());

                //校验
                //u.getRole().forEach(e -> verify(joiner,e));
                //u.setErrorMsg(joiner.toString());
            }



//            //返回
            return users;
        } catch (Exception e) {
            System.out.println("出错了");
            e.printStackTrace();
        }

        return null;
    }

    //使用PoiValidationUtil校验collection中的字段
    private void verify(StringJoiner joiner, Object e) {
        String msg = PoiValidationUtil.validation(e, null);
        if (!StringUtils.isEmpty(msg)){
            joiner.add(msg);
        }


    }

}
