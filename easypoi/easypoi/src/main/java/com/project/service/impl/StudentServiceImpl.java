package com.project.service.impl;

import com.project.pojo.Student;
import com.project.mapper.StudentMapper;
import com.project.service.StudentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 汤小白
 * @since 2020-08-09
 */
@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements StudentService {

}
