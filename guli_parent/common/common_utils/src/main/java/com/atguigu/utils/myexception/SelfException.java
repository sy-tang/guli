package com.atguigu.utils.myexception;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author 汤小白
 * @date 2020/7/1
 */
@Data
@AllArgsConstructor
@NoArgsConstructor

public class SelfException extends RuntimeException {

    @ApiModelProperty(value = "错误状态码")
    private Integer code;

    @ApiModelProperty(value = "错误信息")
    private String msg;

}
