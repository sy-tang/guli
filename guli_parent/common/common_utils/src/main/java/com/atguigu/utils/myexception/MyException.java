package com.atguigu.utils.myexception;

import com.atguigu.utils.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author 汤小白
 * @date 2020/7/1
 */
@ControllerAdvice
@Slf4j
public class MyException  {

    @ExceptionHandler(SelfException.class)
    @ResponseBody
    public Result error(SelfException e){
        log.error(e.getMessage());
        e.printStackTrace();
        return Result.error().message(e.getMsg()).code(e.getCode());
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Result error(Exception e){
        e.printStackTrace();
        return Result.error().message("全局错误");
    }



}
