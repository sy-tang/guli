package com.atguigu.utils;

/**
 * @author 汤小白
 * @date 2020/7/1
 */
public interface ResultCode {
    public static final  Integer SUCCESS=20000;
    public static final  Integer ERROR=20001;
}
