package com.atguigu.utils;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 汤小白
 * @date 2020/7/1
 * 设置同一的结果返回
 */
@Data
public class Result {

    @ApiModelProperty(value = "是否成功")
    private Boolean success;

    @ApiModelProperty(value = "返回码")
    private Integer code;

    @ApiModelProperty(value = "返回消息")
    private String message;

    @ApiModelProperty(value = "返回数据")
    private Map<String,Object> data=new HashMap<>();

    private Result(){}  //构造器私有

    public static Result ok(){
        Result result = new Result();
        result.setSuccess(true);
        result.setCode(ResultCode.SUCCESS);
        result.setMessage("成功");
        return result;
    }

    public static Result error(){
        Result result = new Result();
        result.setSuccess(false);
        result.setCode(ResultCode.ERROR);
        result.setMessage("失败");
        return result;
    }

    public Result success(Boolean success){
        this.success=success;
        return this;
    }

    public Result code(Integer code){
        this.code=code;
        return this;
    }

    public Result message(String message){
        this.message=message;
        return this;
    }

    public Result data(String key,Object val){
        this.data.put(key,val);
        return this;
    }

    public Result data(Map map){
        this.setData(map);
        return this;
    }
}
