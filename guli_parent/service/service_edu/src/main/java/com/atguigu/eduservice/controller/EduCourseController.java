package com.atguigu.eduservice.controller;


import com.atguigu.eduservice.entity.EduCourse;
import com.atguigu.eduservice.entity.EduCourseVo;
import com.atguigu.eduservice.entity.chapter.ChapterVo;
import com.atguigu.eduservice.entity.vo.CourseConditionVo;
import com.atguigu.eduservice.entity.vo.CoursePublishVo;
import com.atguigu.eduservice.service.EduCourseService;
import com.atguigu.utils.Result;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author 汤小白
 * @since 2020-07-04
 * 课程基本信息
 */
@RestController
@RequestMapping("/eduservice/course")
@CrossOrigin
@Api(description = "课程 前端控制器")
public class EduCourseController {

    @Autowired
    EduCourseService eduCourseService;

    @PostMapping("saveCourse")
    @ApiOperation("添加课程信息")
    public Result saveCourse(@RequestBody EduCourseVo courseVo){
        String id = eduCourseService.saveCourse(courseVo);
        return Result.ok().data("courseId",id);
    }

    @ApiOperation("回显课程信息")
    @GetMapping("getInfoById/{courseId}")
    public Result getCourseInfoById(@PathVariable String courseId){
        EduCourseVo courseVo = eduCourseService.getInfoById(courseId);
        return Result.ok().data("course",courseVo);
    }

    @ApiOperation("修改课程基本信息")
    @PostMapping("updCourseInfo")
    public Result  updCourseInfo(@RequestBody EduCourseVo courseVo){
        eduCourseService.updCourseInfo(courseVo);
        return Result.ok();
    }

    @ApiOperation("获取最终发布课程信息")
    @GetMapping("getPublish/{id}")
    public Result getCoursePublish(@PathVariable("id")  String courseId){
        CoursePublishVo publishInfo = eduCourseService.getCoursePublish(courseId);
        return Result.ok().data("publishInfo",publishInfo);
    }

    @PostMapping("updStatus/{id}")
    public Result updCouresStatus(@PathVariable String id){
        EduCourse eduCourse = new EduCourse();
        eduCourse.setId(id);
        eduCourse.setStatus("Normal");
        boolean b = eduCourseService.updateById(eduCourse);
        if (b){
            return Result.ok();
        }else {
            return Result.error();
        }
    }

    /**
     * 多条件分页查询
     * @param conditionVo
     * @param current
     * @param limit
     * @return
     */
    @PostMapping("pageCourseCondition/{current}/{limit}")
    public Result findCoursePage(
            @RequestBody CourseConditionVo conditionVo,
            @PathVariable Long current,
            @PathVariable Long limit){
        Page<EduCourse> page =  eduCourseService.getCourseCondition(conditionVo,current,limit);
        return  Result.ok().data("total",page.getTotal()).data("items",page.getRecords());
    }

    /**
     * 删除课程，并且删除课程下的所有信息
     * @param id 课程id
     * @return
     */
    @DeleteMapping("delCourse/{id}")
    public Result delCourse(@PathVariable String id){
        Boolean flag = eduCourseService.delCourse(id);
        if (flag){
            return Result.ok();
        }else {
            return Result.error();
        }

    }
}

