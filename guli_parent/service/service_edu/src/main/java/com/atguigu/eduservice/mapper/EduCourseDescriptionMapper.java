package com.atguigu.eduservice.mapper;

import com.atguigu.eduservice.entity.EduCourseDescription;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程简介 Mapper 接口
 * </p>
 *
 * @author 汤小白
 * @since 2020-07-04
 */
public interface EduCourseDescriptionMapper extends BaseMapper<EduCourseDescription> {

}
