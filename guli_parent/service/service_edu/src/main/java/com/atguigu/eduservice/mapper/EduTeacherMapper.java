package com.atguigu.eduservice.mapper;

import com.atguigu.eduservice.entity.EduTeacher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 讲师 Mapper 接口
 * </p>
 *
 * @author 汤小白
 * @since 2020-07-01
 */
public interface EduTeacherMapper extends BaseMapper<EduTeacher> {

}
