package com.atguigu.eduservice.service;

import com.atguigu.eduservice.entity.EduTeacher;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 讲师 服务类
 * </p>
 *
 * @author 汤小白
 * @since 2020-07-01
 */
public interface EduTeacherService extends IService<EduTeacher> {

    List<EduTeacher> getTeacherLimit();
}
