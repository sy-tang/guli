package com.atguigu.eduservice.service.impl;

import com.atguigu.eduservice.client.VodClient;
import com.atguigu.eduservice.entity.EduVideo;
import com.atguigu.eduservice.mapper.EduVideoMapper;
import com.atguigu.eduservice.service.EduVideoService;
import com.atguigu.utils.myexception.SelfException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程视频 服务实现类
 * </p>
 *
 * @author 汤小白
 * @since 2020-07-04
 */
@Service
public class EduVideoServiceImpl extends ServiceImpl<EduVideoMapper, EduVideo> implements EduVideoService {

    @Autowired
    VodClient vodClient;
    /**
     * 根据课程id删除所有小节，并且删除所有视频
     * @param id 课程id
     * @return
     */
    public void delVideoByCourseId(String id){
        //查询所有视频id
        QueryWrapper<EduVideo> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id",id);
        wrapper.select("video_source_id");  //查询所有视频id
        List<EduVideo> eduVideos = baseMapper.selectList(wrapper);
        List<String> vodIds = new ArrayList<>();
        for (int i = 0; i < eduVideos.size(); i++) {
            if (eduVideos.get(i)!=null){  //判断对是否为空，不为空则一定有视频id
                String vodId = eduVideos.get(i).getVideoSourceId();
                vodIds.add(vodId);
            }
        }
        //调用vod中的模块
        if (vodIds.size()>0){
            vodClient.delVodBatch(vodIds);
        }
        QueryWrapper<EduVideo> wr = new QueryWrapper<>();
        wr.eq("course_id",id);
        baseMapper.delete(wr);
    }

}
