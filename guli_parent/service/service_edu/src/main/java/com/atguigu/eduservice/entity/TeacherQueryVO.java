package com.atguigu.eduservice.entity;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 汤小白
 * @date 2020/7/1
 */
@Data
@Api(value = "Teachar多条件查询" ,description = "查询对象的封装")
public class TeacherQueryVO implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "教师名称，模糊查询")
    private String name;

    @ApiModelProperty(value = "头衔 1高级讲师  2首席讲师")
    private Integer level;

    @ApiModelProperty(value = "查询开始时间" ,example = "2019-01-01 10:10:10")
    private String begin;

    @ApiModelProperty(value = "查询结束时间",example = "2019-01-01 10:10:10")
    private String end;
}
