package com.atguigu.eduservice.controller;


import com.atguigu.eduservice.entity.EduChapter;
import com.atguigu.eduservice.entity.chapter.ChapterVo;
import com.atguigu.eduservice.service.EduChapterService;
import com.atguigu.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author 汤小白
 * @since 2020-07-04
 * 课程大纲
 */
@RestController
@RequestMapping("/eduservice/chapter")
@CrossOrigin
@Api(description = "课程大纲模块")
public class EduChapterController {

    @Autowired
    EduChapterService eduChapterService;

    /**
     * 获取课程大纲（章节和小节）
     * @param courseId
     * @return
     */
    @GetMapping("/getAllChapter/{courseId}")
    @ApiOperation("获取课程大纲")
    public Result getChapter(@PathVariable String courseId){
        List<ChapterVo> list= eduChapterService.getChapterList(courseId);

        return Result.ok().data("list",list);
    }


    /**
     * 添加章节
     * @param eduChapter
     * @return
     */
    @ApiOperation("添加章节")
    @PostMapping("addChapter")
    public Result addChapter(@RequestBody EduChapter eduChapter){
        eduChapterService.save(eduChapter);
        return Result.ok();
    }

    /**
     * 通过id查询章节
     * @param id
     * @return
     */
    @ApiOperation("根据id查询章节")
    @GetMapping("getChapterById/{id}")
    public Result getChapterById(@PathVariable String id){
        EduChapter chapter = eduChapterService.getById(id);
        return Result.ok().data("chapter",chapter);
    }

    /**
     * 删除章节
     * @param id
     * @return
     */
    @DeleteMapping("{id}")
    public Result delChapter(@PathVariable String id){
       boolean flag = eduChapterService.delChapter(id);
        if(flag){
            return Result.ok();
        }else {
            return Result.error();
        }
    }

    /**
     * 修改章节
     * @param chapter
     * @return
     */
    @PostMapping("updChapter")
    public Result updChapter(@RequestBody EduChapter chapter){
        eduChapterService.updateById(chapter);
        return Result.ok();
    }

}

