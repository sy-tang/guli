package com.atguigu.eduservice.controller.font;

import com.atguigu.eduservice.entity.EduCourse;
import com.atguigu.eduservice.service.EduCourseService;
import com.atguigu.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 汤小白
 * @date 2020/7/9
 */
@RestController
@RequestMapping("/eduservice/font/course")
@CrossOrigin
@Api(description = "前台获取课程")
public class EduCourseFontController {
    @Autowired
    EduCourseService eduCourseService;

    /**
     * 前台获取课程，只获取8条
     * @return
     */
    @ApiOperation("前台获取课程")
    @GetMapping("getFontCourse")
    public Result getFontCourse(){
      List<EduCourse> list = eduCourseService.getCourseLimit();
        return Result.ok().data("courseList",list);
    }
}
