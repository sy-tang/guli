package com.atguigu.eduservice.controller;


import com.atguigu.eduservice.EduApplication;
import com.atguigu.eduservice.entity.EduTeacher;
import com.atguigu.eduservice.entity.TeacherQueryVO;
import com.atguigu.eduservice.service.EduTeacherService;
import com.atguigu.utils.Result;
import com.atguigu.utils.myexception.SelfException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author 汤小白
 * @since 2020-07-01
 */
@RestController
@RequestMapping("/eduservice/teacher")
@Api(description = "讲师管理")
@CrossOrigin
public class EduTeacherController {


    @Autowired
    EduTeacherService eduTeacherService;

    /**
     * 查询所有讲师
     *
     * @return list
     */
    @ApiOperation(value = "所有讲师列表")
    @GetMapping("findall")
    public Result findAllTeacher() {
        List<EduTeacher> list = eduTeacherService.list(null);
        //测试自定义异常
        //try {
        //    int i=10/0;
        //} catch (Exception e) {
        //   throw  new SelfException(500,"自定义异常");
        //}
        return Result.ok().data("items", list);
    }


    @ApiOperation(value = "根据id删除讲师")
    @DeleteMapping("{id}")
    public Result removeTeacher(
            @ApiParam(name = "id", value = "讲师id", required = true)
            @PathVariable("id") String id) {
        boolean flag = eduTeacherService.removeById(id);
        if (flag) {
            return Result.ok();
        } else {
            return Result.error();
        }
    }


    /**
     * 讲师分页查询
     *
     * @param current 当前页码
     * @param limit   每页大小
     * @return 同一返回类型
     */
    @ApiOperation(value = "讲师分页")
    @GetMapping("/pageTeacher/{current}/{limit}")
    public Result pageTeacher(
            @ApiParam(name = "current", value = "当前页码", required = true)
            @PathVariable Long current,
            @ApiParam(name = "limit", value = "每页记录数", required = true)
            @PathVariable Long limit) {

        Page<EduTeacher> page = new Page<>(current, limit);
        eduTeacherService.page(page, null);  //分页查询，数据封装在page中
        long total = page.getTotal();
        List<EduTeacher> records = page.getRecords();
        return Result.ok().data("total", total).data("items", records);
    }


    /**
     * 多条件分页查询
     *
     * @param current        当前页码
     * @param limit          每页大小
     * @param teacherQueryVO
     * @return
     * @see com.atguigu.eduservice.entity.TeacherQueryVO  封装的条件对象
     */
    @ApiOperation(value = "讲师多条件分页")
    @PostMapping("/pageTeacherCondition/{current}/{limit}")  //使用RequestBody就必须使用post，不能get请求
    public Result pageTeacherCondition(
            @ApiParam(name = "current", value = "当前页码", required = true)
            @PathVariable Long current,
            @ApiParam(name = "limit", value = "每页记录数", required = true)
            @PathVariable Long limit,
            @ApiParam(name = "teacherQueryVO", value = "多条件对象", required = false)
            @RequestBody(required = false) TeacherQueryVO teacherQueryVO) {
        // 构建条件
        QueryWrapper<EduTeacher> wrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(teacherQueryVO)) {
            if (!StringUtils.isEmpty(teacherQueryVO.getName())) {
                wrapper.like("name", teacherQueryVO.getName());
            }
            if (!StringUtils.isEmpty(teacherQueryVO.getLevel())) {
                wrapper.eq("level", teacherQueryVO.getLevel());
            }
            if (!StringUtils.isEmpty(teacherQueryVO.getBegin())) {
                wrapper.ge("gmt_create", teacherQueryVO.getBegin());
            }
            if (!StringUtils.isEmpty(teacherQueryVO.getEnd())) {
                wrapper.le("gmt_create", teacherQueryVO.getEnd());
            }
        }
        wrapper.orderByDesc("gmt_create");
        Page<EduTeacher> page = new Page<>(current, limit);
        eduTeacherService.page(page, wrapper);//分页查询，数据封装在page中
        long total = page.getTotal();
        List<EduTeacher> records = page.getRecords();
        return Result.ok().data("total", total).data("items", records);
    }

    /**
     * 插入Teacher
     *
     * @param eduTeacher
     * @return
     */
    @ApiOperation(value = "插入Teacher")
    @PostMapping("/saveTeacher")
    public Result save(
            @ApiParam(name = "eduTeacher", value = "teacher对象", required = true)
            @RequestBody EduTeacher eduTeacher) {
        boolean save = eduTeacherService.save(eduTeacher);
        if (save) {
            return Result.ok();
        } else {
            return Result.error();
        }

    }

    /**
     * 根据id获取讲师记录
     * @param id
     * @return
     */
    @ApiOperation(value = "根据id获取讲师记录")
    @GetMapping("/getTeacherById/{id}")
    public Result getTeacherById(
            @ApiParam(name = "id", value = "讲师id", required = true)
            @PathVariable("id") String id) {
        EduTeacher teacher = eduTeacherService.getById(id);
            return Result.ok().data("item",teacher);
    }

    /**
     * 根据id修改讲师信息
     * @param id
     * @param teacher
     * @return
     */
    @ApiOperation(value = "根据id修改讲师信息")
    //修改本来应该使用put方法的，当使用put时，id需要单独获取以及设置到讲师对象中。
    @PostMapping("/updateById")
    public Result updateById(
            @ApiParam(name = "teacher",value = "讲师对象",required = true)
            @RequestBody EduTeacher teacher){
        boolean flag = eduTeacherService.updateById(teacher);
        if (flag){
            return Result.ok();
        }else {
            return Result.error();
        }

    }

}

