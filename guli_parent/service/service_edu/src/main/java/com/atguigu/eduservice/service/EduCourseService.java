package com.atguigu.eduservice.service;

import com.atguigu.eduservice.entity.EduCourse;
import com.atguigu.eduservice.entity.EduCourseVo;
import com.atguigu.eduservice.entity.vo.CourseConditionVo;
import com.atguigu.eduservice.entity.vo.CoursePublishVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author 汤小白
 * @since 2020-07-04
 */
public interface EduCourseService extends IService<EduCourse> {

    String saveCourse(EduCourseVo courseVo);

    EduCourseVo getInfoById(String courseId);

    void updCourseInfo(EduCourseVo courseVo);

    CoursePublishVo getCoursePublish(String courseId);

    Page<EduCourse> getCourseCondition(CourseConditionVo conditionVo, Long current, Long limit);

    Boolean delCourse(String id);

    List<EduCourse> getCourseLimit();
}
