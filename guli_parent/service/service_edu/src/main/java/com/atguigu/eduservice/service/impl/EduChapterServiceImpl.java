package com.atguigu.eduservice.service.impl;

import com.atguigu.eduservice.entity.EduChapter;
import com.atguigu.eduservice.entity.EduVideo;
import com.atguigu.eduservice.entity.chapter.ChapterVo;
import com.atguigu.eduservice.entity.chapter.VideoVo;
import com.atguigu.eduservice.mapper.EduChapterMapper;
import com.atguigu.eduservice.service.EduChapterService;
import com.atguigu.eduservice.service.EduVideoService;
import com.atguigu.utils.myexception.SelfException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author 汤小白
 * @since 2020-07-04
 */
@Service
public class EduChapterServiceImpl extends ServiceImpl<EduChapterMapper, EduChapter> implements EduChapterService {

    @Autowired
    EduVideoService eduVideoService;

    @Override
    public List<ChapterVo> getChapterList(String courseId) {

        //查出所有章节
        QueryWrapper<EduChapter> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id",courseId);
        List<EduChapter> eduChapters = baseMapper.selectList(wrapper);
        //查出所有小结
        QueryWrapper<EduVideo> wrapperVideo = new QueryWrapper<>();
        wrapperVideo.eq("course_id",courseId);
        List<EduVideo> videoList = eduVideoService.list(wrapperVideo);

        //最终保存的集合
        List<ChapterVo> finallList = new ArrayList<>();
        //整合
        for (int i = 0; i <eduChapters.size() ; i++) {

            EduChapter eduChapter= eduChapters.get(i);
            ChapterVo chapterVo = new ChapterVo();
            BeanUtils.copyProperties(eduChapter,chapterVo);
            finallList.add(chapterVo);

            ArrayList<VideoVo> videos = new ArrayList<>();
            for (int m = 0; m < videoList.size(); m++) {
                if (videoList.get(m).getChapterId().equals(eduChapter.getId())){
                    EduVideo eduVideo = videoList.get(m);
                    VideoVo videoVo = new VideoVo();
                    BeanUtils.copyProperties(eduVideo,videoVo);
                    videos.add(videoVo);
                }
            }
            chapterVo.setChildren(videos);
        }
        return finallList;
    }

    /**
     * 删除章节，如果章节下有小节，则不能删除
     * @param id
     * @return
     */
    @Override
    public boolean delChapter(String id) {
        QueryWrapper<EduVideo> wrapper = new QueryWrapper<>();
        wrapper.eq("chapter_id",id);
        int count = eduVideoService.count(wrapper);
        if (count>0){  //章节下面有小节，不能删除
            throw  new SelfException(20001,"请先删除章节下的小节");
        }
        int i = baseMapper.deleteById(id);  //删除章节
        return i>0;
    }

    /**
     * 删除课程下的所有章节
     * @param id
     */
    public void delChapterByCourseId(String id){
        QueryWrapper<EduChapter> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id",id);
         baseMapper.delete(wrapper);
    }
}
