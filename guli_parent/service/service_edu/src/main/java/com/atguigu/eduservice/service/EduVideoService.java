package com.atguigu.eduservice.service;

import com.atguigu.eduservice.entity.EduVideo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程视频 服务类
 * </p>
 *
 * @author 汤小白
 * @since 2020-07-04
 */
public interface EduVideoService extends IService<EduVideo> {
     void delVideoByCourseId(String id);
}
