package com.atguigu.eduservice.service.impl;

import com.alibaba.excel.EasyExcel;
import com.atguigu.eduservice.entity.EduSubject;
import com.atguigu.eduservice.entity.excel.SubjectData;
import com.atguigu.eduservice.entity.subject.OneSubject;
import com.atguigu.eduservice.entity.subject.TwoSubject;
import com.atguigu.eduservice.listener.SubjectListener;
import com.atguigu.eduservice.mapper.EduSubjectMapper;
import com.atguigu.eduservice.service.EduSubjectService;
import com.atguigu.utils.myexception.SelfException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程科目 服务实现类
 * </p>
 *
 * @author 汤小白
 * @since 2020-07-03
 */
@Service
public class EduSubjectServiceImpl extends ServiceImpl<EduSubjectMapper, EduSubject> implements EduSubjectService {

    /**
     * 添加课程分类
     * @param file
     * @param eduSubjectService
     */
    @Override
    public void saveSubject(MultipartFile file, EduSubjectService eduSubjectService) {

        //读取课程分类信息
        try {
            InputStream inputStream = file.getInputStream();
            EasyExcel.read(inputStream, SubjectData.class,new SubjectListener(eduSubjectService)).sheet().doRead();
        } catch (IOException e) {
            e.printStackTrace();
            throw new SelfException(20001,"添加分类失败");
        }

    }

    /**
     * 查询课程列表
     * @return
     */
    @Override
    public List<OneSubject> getSbuject() {
        //查询一级分类
        QueryWrapper<EduSubject> oneWrapper = new QueryWrapper<>();
        oneWrapper.eq("parent_id","0");
        List<EduSubject> oneList = baseMapper.selectList(oneWrapper);
        //查询二级分类
        QueryWrapper<EduSubject> twoWrapper = new QueryWrapper<>();
        twoWrapper.ne("parent_id","0");
        List<EduSubject> twoList = baseMapper.selectList(twoWrapper);

        //最终存放一二级分类的集合
        List<OneSubject> finallyList = new ArrayList<>();
        //整合
        for (int i = 0; i <oneList.size() ; i++) {
            //将一级分类放进去
            OneSubject oneSubject = new OneSubject();
            EduSubject eduSubject = oneList.get(i);
            BeanUtils.copyProperties(eduSubject,oneSubject);
            finallyList.add(oneSubject);

            //暂时存放二级分类的list
            List<TwoSubject> twoSubjects = new ArrayList<>();
            //将二级分类放到一级分类中
            for (int m = 0; m < twoList.size(); m++) {
                TwoSubject twoSubject = new TwoSubject();
                EduSubject tSubject = twoList.get(m);
                if (tSubject.getParentId().equals(eduSubject.getId())){  //放到对应的一级分类中
                    BeanUtils.copyProperties(tSubject,twoSubject);
                    twoSubjects.add(twoSubject);
                }
            }
            oneSubject.setChildren(twoSubjects);
        }
        return finallyList;
    }
}
