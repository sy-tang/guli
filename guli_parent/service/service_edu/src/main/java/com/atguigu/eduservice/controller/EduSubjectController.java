package com.atguigu.eduservice.controller;


import com.atguigu.eduservice.entity.EduSubject;
import com.atguigu.eduservice.entity.subject.OneSubject;
import com.atguigu.eduservice.service.EduSubjectService;
import com.atguigu.utils.Result;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 课程科目 前端控制器
 * </p>
 *
 * @author 汤小白
 * @since 2020-07-03
 */
@RestController
@RequestMapping("/eduservice/subject")
@CrossOrigin
public class EduSubjectController {

    @Autowired
    EduSubjectService eduSubjectService;

    /**
     * 导入课程分类
     * @param file
     * @return
     */
    @ApiOperation("导入课程分类")
    @PostMapping("saveSubject")
    public Result saveSubject(
            @ApiParam(name = "file",value = "xls类型的文件",required = true)
            MultipartFile file){
        eduSubjectService.saveSubject(file,eduSubjectService);
        return Result.ok();
    }


    /**
     * 显示课程列表
     * @return
     */
    @ApiOperation("显示课程列表")
    @GetMapping("getSubjectList")
    public Result getSubject(){
        List<OneSubject> list = eduSubjectService.getSbuject();
        return Result.ok().data("list",list);
    }


}

