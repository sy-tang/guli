package com.atguigu.eduservice.service.impl;

import com.atguigu.eduservice.entity.EduCourse;
import com.atguigu.eduservice.entity.EduCourseDescription;
import com.atguigu.eduservice.entity.EduCourseVo;
import com.atguigu.eduservice.entity.EduVideo;
import com.atguigu.eduservice.entity.vo.CourseConditionVo;
import com.atguigu.eduservice.entity.vo.CoursePublishVo;
import com.atguigu.eduservice.mapper.EduCourseMapper;
import com.atguigu.eduservice.service.EduChapterService;
import com.atguigu.eduservice.service.EduCourseDescriptionService;
import com.atguigu.eduservice.service.EduCourseService;
import com.atguigu.eduservice.service.EduVideoService;
import com.atguigu.utils.myexception.SelfException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author 汤小白
 * @since 2020-07-04
 */
@Service
public class EduCourseServiceImpl extends ServiceImpl<EduCourseMapper, EduCourse> implements EduCourseService {

    @Autowired
    EduCourseDescriptionService descriptionService;

    @Autowired
    EduVideoService eduVideoService;

    @Autowired
    EduChapterService eduChapterService;
    /**
     * 添加课程基本信息
     * @param courseVo
     * @return
     */
    @Override
    public String saveCourse(EduCourseVo courseVo) {
        //添加课程基本信息
        EduCourse eduCourse = new EduCourse();
        BeanUtils.copyProperties(courseVo,eduCourse);
        int insert = baseMapper.insert(eduCourse);
        if (insert <=0){
            throw new SelfException(20001,"添加课程基本信息失败");
        }
        //添加成功后或者id，用于与课程简介进行一对一
        String cid = eduCourse.getId();

        //添加课程简介
        EduCourseDescription description = new EduCourseDescription();
        BeanUtils.copyProperties(courseVo,description);
        description.setId(cid);
        descriptionService.save(description);
        return cid;
    }

    /**
     * 根据课程id获取课程基本信息
     * @param courseId
     * @return
     */
    @Override
    public EduCourseVo getInfoById(String courseId) {
        //查询出课程信息
        EduCourse course = baseMapper.selectById(courseId);
        EduCourseVo courseVo = new EduCourseVo();
        BeanUtils.copyProperties(course,courseVo);

        //查询课程简介
        EduCourseDescription description = descriptionService.getById(course.getId());
        courseVo.setDescription(description.getDescription());

        return courseVo;
    }

    /**
     * 更新课程信息
     * @param courseVo
     */
    @Override
    public void updCourseInfo(EduCourseVo courseVo) {
        //修改基本信息
        EduCourse eduCourse = new EduCourse();
        BeanUtils.copyProperties(courseVo,eduCourse);
        int i = baseMapper.updateById(eduCourse);

        if (i<=0){
            throw new SelfException(20001,"课程信息更新失败");
        }

        EduCourseDescription description = new EduCourseDescription();
        description.setId(courseVo.getId());
        description.setDescription(courseVo.getDescription());
        //修改课程简介
        descriptionService.updateById(description);
    }

    /**
     * 查询最终发布的课程信息
     * @param courseId
     * @return
     */
    public CoursePublishVo getCoursePublish(String courseId){
        CoursePublishVo coursePublish = baseMapper.getCoursePublish(courseId);
        return coursePublish;
    }

    /**
     * 多条件分页查询
     * @param conditionVo
     * @return
     */
    @Override
    public Page<EduCourse> getCourseCondition(CourseConditionVo conditionVo, Long current, Long limit) {
        QueryWrapper<EduCourse> wrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(conditionVo)){
            if (!StringUtils.isEmpty(conditionVo.getTitle())){
                wrapper.eq("title",conditionVo.getTitle());
            }
            if (!StringUtils.isEmpty(conditionVo.getStatus())){
                String status=conditionVo.getStatus();
               status=status.equals("1")?"Normal":"Draft";
                wrapper.eq("status",status);
            }
            wrapper.orderByDesc("gmt_create");
        }
        Page<EduCourse> page = new Page<>(current,limit);
        baseMapper.selectPage(page,wrapper);
        return page;
    }

    /**
     * 删除课程，先要删除所有课程小节，课程章节、课程描述
     * @param id
     * @return
     */
    @Override
    public Boolean delCourse(String id) {
        //删除课程小节
         eduVideoService.delVideoByCourseId(id);
        //删除课程章节
        eduChapterService.delChapterByCourseId(id);
        //删除课程简介
        descriptionService.removeById(id);
        //删除课程基本信息
        int i = baseMapper.deleteById(id);
        return i>0;
    }

    /**
     * 前台获取课程，只获取8条记录
     * @return
     */
    @Override
    public List<EduCourse> getCourseLimit() {
        QueryWrapper<EduCourse> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("view_count");
        wrapper.last("limit 8");
        List<EduCourse> list = baseMapper.selectList(wrapper);
        return list;
    }
}
