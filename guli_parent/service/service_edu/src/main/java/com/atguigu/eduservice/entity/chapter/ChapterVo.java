package com.atguigu.eduservice.entity.chapter;

import com.atguigu.eduservice.entity.subject.TwoSubject;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 汤小白
 * @date 2020/7/5
 */
@Data
public class ChapterVo {
    private String id;

    private String title;

    private List<VideoVo> children=new ArrayList<>();
}
