package com.atguigu.eduservice.client;

import com.atguigu.eduservice.client.impl.VodClientImpl;
import com.atguigu.utils.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author 汤小白
 * @date 2020/7/8
 * 调用视频模块
 */
@FeignClient(value = "service-vod" ,fallback = VodClientImpl.class)  //fallback 兜底方法
@Component
public interface VodClient {

    @DeleteMapping("/vod/delVod/{id}")   //路径一定要写全
     Result delVod(@PathVariable("id") String id);

    @DeleteMapping("/vod/delVodBatch")
    Result delVodBatch(@RequestParam("vodIds") List<String> vodIds);
}
