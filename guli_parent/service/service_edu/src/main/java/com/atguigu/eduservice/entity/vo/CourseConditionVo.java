package com.atguigu.eduservice.entity.vo;

import lombok.Data;

/**
 * @author 汤小白
 * @date 2020/7/7
 */
@Data
public class CourseConditionVo {

    //课程名称
    private String title;

    //课程状态
    private String status;
}
