package com.atguigu.eduservice.client.impl;

import com.atguigu.eduservice.client.VodClient;
import com.atguigu.utils.Result;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author 汤小白
 * @date 2020/7/8
 */
@Component  //熔断时候的兜底方法，需要交给容器处理
public class VodClientImpl implements VodClient {
    @Override
    public Result delVod(String id) {
        return Result.error().message("删除小节视频失败");
    }

    @Override
    public Result delVodBatch(List<String> vodIds) {
        return Result.error().message("批量删除视频失败");
    }
}
