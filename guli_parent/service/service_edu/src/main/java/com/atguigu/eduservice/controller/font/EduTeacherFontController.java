package com.atguigu.eduservice.controller.font;

import com.atguigu.eduservice.entity.EduTeacher;
import com.atguigu.eduservice.service.EduTeacherService;
import com.atguigu.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 汤小白
 * @date 2020/7/9
 */
@RestController
@RequestMapping("/eduservice/font/teacher")
@CrossOrigin
@Api(description = "前台讲师")
public class EduTeacherFontController {
    @Autowired
    EduTeacherService eduTeacherService;

    /**
     * 前台获取推荐讲师
     * @return
     */
    @ApiOperation("前台获取推荐讲师")
    @GetMapping("getFontTeacher")
    public Result getFontTeacher(){
        List<EduTeacher> list = eduTeacherService.getTeacherLimit();
        return Result.ok().data("teacherList",list);
    }
}
