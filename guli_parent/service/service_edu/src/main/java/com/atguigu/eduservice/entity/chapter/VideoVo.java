package com.atguigu.eduservice.entity.chapter;

import lombok.Data;

/**
 * @author 汤小白
 * @date 2020/7/5
 */
@Data
public class VideoVo {
    private String id;

    private String title;
}
