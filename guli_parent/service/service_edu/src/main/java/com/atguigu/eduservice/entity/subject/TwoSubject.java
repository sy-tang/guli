package com.atguigu.eduservice.entity.subject;

import lombok.Data;

/**
 * @author 汤小白
 * @date 2020/7/4
 */
@Data
public class TwoSubject {


    private String id;

    private String title;
}
