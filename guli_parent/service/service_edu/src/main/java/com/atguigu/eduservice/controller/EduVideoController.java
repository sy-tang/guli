package com.atguigu.eduservice.controller;


import com.atguigu.eduservice.client.VodClient;
import com.atguigu.eduservice.entity.EduVideo;
import com.atguigu.eduservice.service.EduVideoService;
import com.atguigu.utils.Result;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.management.Query;

/**
 * <p>
 * 课程视频 前端控制器
 * </p>
 *
 * @author 汤小白
 * @since 2020-07-04
 */
@RestController
@RequestMapping("/eduservice/video")
@CrossOrigin
@Api(description = "章节小节模块")
public class EduVideoController {

    @Autowired
    EduVideoService eduVideoService;

    @Autowired
    VodClient vodClient;
    /**
     * 添加小节
     * @param video
     * @return
     */
    @PostMapping("/addVideo")
    public Result addVideo(@RequestBody EduVideo video){
        eduVideoService.save(video);
        return Result.ok();
    }


    /**
     * 通过id查询小节
     * @param id
     * @return
     */
    @GetMapping("getVideoById/{id}")
    public Result getVideoById(@PathVariable String id){
        EduVideo video = eduVideoService.getById(id);
        return Result.ok().data("video",video);
    }


    /**
     * 删除小节，并且删除小节对应的视频
     * @param id  小节id
     * @return
     */
    @DeleteMapping("{id}")
    public Result delVideo(@PathVariable String id){
        EduVideo video = eduVideoService.getById(id);
        String sourceId = video.getVideoSourceId();
        if (!StringUtils.isEmpty(sourceId)){ //视频id不为空则删除
            vodClient.delVod(sourceId); //调用vod模块删除视频
        }
        eduVideoService.removeById(id); //删除小节
        return Result.ok();
    }


    /**
     * 修改小节
     * @param video
     * @return
     */
    @PostMapping("updVideo")
    public Result updVideo(@RequestBody EduVideo video) {
        eduVideoService.updateById(video);
        return Result.ok();
    }


}

