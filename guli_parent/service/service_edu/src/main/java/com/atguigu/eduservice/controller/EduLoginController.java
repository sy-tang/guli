package com.atguigu.eduservice.controller;

import com.atguigu.utils.Result;
import com.baomidou.mybatisplus.extension.api.R;
import org.springframework.web.bind.annotation.*;

/**
 * @author 汤小白
 * @date 2020/7/2
 * 登录
 */
@RestController
@RequestMapping("/eduservice/user")
@CrossOrigin
public class EduLoginController {

    @PostMapping("/login")
    public Result login(){
        return Result.ok().data("token","admin");
    }

    @GetMapping("info")
    public Result info(){
        return Result.ok()
                .data("name","admin")
                .data("roles","admin")
                .data("avatar","https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");
    }
}
