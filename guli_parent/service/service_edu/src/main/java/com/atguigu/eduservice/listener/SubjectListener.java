package com.atguigu.eduservice.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.metadata.CellData;
import com.atguigu.eduservice.entity.EduSubject;
import com.atguigu.eduservice.entity.excel.SubjectData;
import com.atguigu.eduservice.service.EduSubjectService;
import com.atguigu.utils.myexception.SelfException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sun.javafx.runtime.eula.Eula;
import org.hibernate.validator.cfg.defs.EANDef;

import java.util.Map;

/**
 * @author 汤小白
 * @date 2020/7/3
 */
/*
SubjectListener无法交给spring容器管理，所以不能在这里使用数据库的操作，只能将service当成
参数传过来，通过service来操作。

注意：中使用EasyExcel时，必须创建.xls或者.xlsx的文件，而不能创建其他类型的文件再重命名为excel文件，这样无法读取。
 */
public class SubjectListener extends AnalysisEventListener<SubjectData> {

    private EduSubjectService eduSubjectService;

    public SubjectListener(EduSubjectService eduSubjectService) {
        this.eduSubjectService = eduSubjectService;
    }

    public SubjectListener() {
    }

    //读取excel表的内容,并将内存保存到数据库中
    @Override
    public void invoke(SubjectData subjectData, AnalysisContext analysisContext) {
        if (subjectData == null) {
            throw new SelfException(20001, "上传的excel表为空");
        }
        //一级分类是否存在
        EduSubject oneSubject = existOneSubject(subjectData.getOneSubjectName());
        if (oneSubject == null) { //如果不存在，则添加
            oneSubject = new EduSubject();
            oneSubject.setParentId("0");
            oneSubject.setTitle(subjectData.getOneSubjectName());
            eduSubjectService.save(oneSubject);
        }

        //判断二级分类是否存在
        //先通过上面的一级分类获取pid
        String pid = oneSubject.getId();
        System.out.println(pid);
        EduSubject twoSubject = existTwoSubject(subjectData.getTwoSubjectName(), pid);
        if (twoSubject == null) { //该二级分类不存在，则添加
            //当分类不存在时，一定要重新new一个，不能使用null.setTitle,会异常
            twoSubject = new EduSubject();
            twoSubject.setTitle(subjectData.getTwoSubjectName());
            twoSubject.setParentId(pid);
            eduSubjectService.save(twoSubject);
        }
    }

    /**
     * 判断一级分类是否相同
     *
     * @param oneSubject
     * @return
     */
    public EduSubject existOneSubject(String oneSubject) {
        QueryWrapper<EduSubject> w = new QueryWrapper<>();
        w.eq("title", oneSubject)
                .eq("parent_id", "0");
        EduSubject one = eduSubjectService.getOne(w);
        return one;
    }

    /**
     * 判断二级分类是否相同
     *
     * @param twoSubject
     * @param pid
     * @return
     */
    public EduSubject existTwoSubject(String twoSubject, String pid) {
        QueryWrapper<EduSubject> w = new QueryWrapper<>();
        w.eq("title", twoSubject)
                .eq("parent_id", pid);
        EduSubject two = eduSubjectService.getOne(w);
        return two;
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }
}
