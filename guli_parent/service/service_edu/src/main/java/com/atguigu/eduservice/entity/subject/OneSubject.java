package com.atguigu.eduservice.entity.subject;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 汤小白
 * @date 2020/7/4
 */
@Data
public class OneSubject {

    private String id;

    private String title;

    private List<TwoSubject> children=new ArrayList<>();
}
