package com.demo.xls;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @author 汤小白
 * @date 2020/7/3
 * excel实体类
 */
@Data
public class SubjectData {
    @ExcelProperty(value = "0",index = 0)
    private String oneSubjectName;

    @ExcelProperty(value = "1",index = 1)
    private String twoSubjectName;
}
