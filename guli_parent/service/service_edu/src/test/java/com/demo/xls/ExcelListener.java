package com.demo.xls;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.metadata.CellData;

import java.util.Map;

/**
 * @author 汤小白
 * @date 2020/7/3
 * 监听读取excel表
 */

public class ExcelListener extends AnalysisEventListener<SubjectData> {
    //读取内容
    @Override
    public void invoke(SubjectData dateDemo, AnalysisContext analysisContext) {
        System.out.println("---"+dateDemo);
    }

    //读取表头
    @Override
    public void invokeHead(Map<Integer, CellData> headMap, AnalysisContext context) {
    }

    //读完之后执行
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }
}
