package com.demo.xls;

import com.alibaba.excel.EasyExcel;

/**
 * @author 汤小白
 * @date 2020/7/3
 */
public class ExcelRead {
    public static void main(String[] args) {
        String file="D:\\02.xlsx";
        EasyExcel.read(file,SubjectData.class,new ExcelListener()).sheet().doRead();
    }
}
