package com.demo.xls;

import com.alibaba.excel.EasyExcel;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 汤小白
 * @date 2020/7/3
 */
public class TestExcel {
    public static void main(String[] args) {
        String fileName = "D:\\02.xlsx";
        EasyExcel.write(fileName, SubjectData.class).sheet("学生列表").doWrite(data());
    }

    public static List<SubjectData>  data(){
        ArrayList<SubjectData> list = new ArrayList<>();
        for (int i = 0; i <10 ; i++) {
            SubjectData dateDemo = new SubjectData();
            dateDemo.setOneSubjectName("one"+i);
            dateDemo.setTwoSubjectName("two"+i);
            list.add(dateDemo);
        }
        return list;
    }
}
