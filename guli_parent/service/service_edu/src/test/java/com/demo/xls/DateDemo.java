package com.demo.xls;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @author 汤小白
 * @date 2020/7/3
 */
@Data
public class DateDemo {

    @ExcelProperty(value = "学生编号",index = 0)
    private Integer sno;

    @ExcelProperty(value = "学生姓名",index = 1)
    private String sname;

}
