package com.atguigu.cmsservice.mapper;

import com.atguigu.cmsservice.entity.CrmBanner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 首页banner表 Mapper 接口
 * </p>
 *
 * @author 汤小白
 * @since 2020-07-09
 */
public interface CrmBannerMapper extends BaseMapper<CrmBanner> {

}
