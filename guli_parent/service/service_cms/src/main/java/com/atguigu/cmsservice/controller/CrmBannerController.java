package com.atguigu.cmsservice.controller;


import com.atguigu.cmsservice.entity.CrmBanner;
import com.atguigu.cmsservice.service.CrmBannerService;
import com.atguigu.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 首页banner表 前端控制器
 * </p>
 *
 * @author 汤小白
 * @since 2020-07-09
 */
@RestController
@RequestMapping("/cmsservice/banner")
@CrossOrigin
@Api(description = "首页轮播图")
public class CrmBannerController {

    @Autowired
    CrmBannerService crmBannerService;

    @ApiOperation("首页获取轮播图")
    @GetMapping("getBanner")
    public Result getBanner(){
      List<CrmBanner> list =  crmBannerService.selBannerList();
        return Result.ok().data("bannerList",list);
    }
}

