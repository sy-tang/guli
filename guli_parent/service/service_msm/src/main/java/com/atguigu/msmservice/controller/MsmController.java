package com.atguigu.msmservice.controller;

import com.atguigu.msmservice.service.MsmService;
import com.atguigu.msmservice.utils.RandomUtil;
import com.atguigu.utils.Result;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

/**
 * @author 汤小白
 * @date 2020/7/10
 */
@RestController
@RequestMapping("/edumsm/msm")
@CrossOrigin
public class MsmController {
    @Autowired
    MsmService msmService;
    @Autowired
    RedisTemplate<String ,String> redisTemplate;

    @ApiOperation("发送短信")
    @GetMapping("/send/{phone}")
    public Result sendMsm(@PathVariable String phone){
        //先判断redis中是否存在phone，存在则不重复发送
        String code = redisTemplate.opsForValue().get(phone);
        if (!StringUtils.isEmpty(code)){ //存在
            return Result.ok().message("短信无需重复发送");
        }
        //生成验证码
        code = RandomUtil.getFourBitRandom();
        HashMap<String, Object> map = new HashMap<>();
        map.put("code",code);
        boolean isSend = msmService.send(phone,map);
        if (isSend){
            return Result.ok();
        }else {
            return Result.error();
        }
    }
}
