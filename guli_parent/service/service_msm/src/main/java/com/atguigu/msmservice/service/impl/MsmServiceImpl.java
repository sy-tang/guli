package com.atguigu.msmservice.service.impl;

import com.atguigu.msmservice.service.MsmService;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * @author 汤小白
 * @date 2020/7/10
 */
@Service
public class MsmServiceImpl implements MsmService {
    @Value("${keyId}")
    String keyId;
    @Value("${secret}")
    String secret;
    @Value("${signName}")
    String signName;
    @Value("${templateCode}")
    String templateCode;

    /**
     * 发送短信
     *
     * @param phone
     * @param code
     * @return
     */
    @Override
    public boolean send(String phone, HashMap<String, Object> code) {

        //连接阿里云
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", keyId, secret);
        IAcsClient client = new DefaultAcsClient(profile);
        //构建请求
        CommonRequest request = new CommonRequest();
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");  //不能改
        request.setVersion("2017-05-25");  //不能改
        request.setAction("SendSms"); //设置事件名称(发送短信)

        //自定义的参数（手机号、验证码、签名、模板）
        //request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", phone);//手机号
        request.putQueryParameter("SignName", signName);  //签名
        request.putQueryParameter("TemplateCode", templateCode);
        //构建短信的验证码
//        HashMap<String, Object> map = new HashMap<>();
//        map.put("code",1234);
        request.putQueryParameter("TemplateParam", JSONObject.toJSONString(code)); //将验证码放入
        try {
            CommonResponse response = client.getCommonResponse(request);//发送，获取响应结果
            System.out.println(response.getData());
            return response.getHttpResponse().isSuccess();
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }
        return false;
    }
}


