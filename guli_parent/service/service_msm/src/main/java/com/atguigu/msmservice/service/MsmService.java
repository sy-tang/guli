package com.atguigu.msmservice.service;

import java.util.HashMap;

/**
 * @author 汤小白
 * @date 2020/7/10
 */
public interface MsmService {
    boolean send(String phone, HashMap<String, Object> code);
}
