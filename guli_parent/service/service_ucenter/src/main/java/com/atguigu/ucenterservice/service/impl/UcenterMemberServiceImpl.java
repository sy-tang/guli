package com.atguigu.ucenterservice.service.impl;

import com.alibaba.nacos.common.util.Md5Utils;
import com.atguigu.ucenterservice.entity.UcenterMember;
import com.atguigu.ucenterservice.entity.vo.RegisterVo;
import com.atguigu.ucenterservice.mapper.UcenterMemberMapper;
import com.atguigu.ucenterservice.service.UcenterMemberService;
import com.atguigu.utils.MD5;
import com.atguigu.utils.jwt.JwtUtils;
import com.atguigu.utils.myexception.SelfException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员表 服务实现类
 * </p>
 *
 * @author 汤小白
 * @since 2020-07-10
 */
@Service
public class UcenterMemberServiceImpl extends ServiceImpl<UcenterMemberMapper, UcenterMember> implements UcenterMemberService {

    @Autowired
    RedisTemplate<String,String> redisTemplate;
    
    /**
     * 用户登录，生成token
     * @param ucenterMember
     * @return
     */
    @Override
    public String login(UcenterMember ucenterMember) {
        //校验是否为空
        String mobile = ucenterMember.getMobile();
        String password = ucenterMember.getPassword();
        if (StringUtils.isEmpty(mobile) || StringUtils.isEmpty(password)){
            throw new SelfException(20001,"登录失败，账号密码不能为空");
        }
        //查询用户
        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile",mobile);
        UcenterMember member = baseMapper.selectOne(wrapper);

        if (null == member){
            throw new SelfException(20001,"用户不存在");
        }

        if (!MD5.encrypt(password).equals(member.getPassword())){
            throw new SelfException(20001,"用户密码错误");
        }

        if (member.getIsDisabled()){
            throw new SelfException(20001,"此用户被禁用");
        }
        String token = JwtUtils.getJwtToken(member.getId(),member.getNickname());
        return token;
    }

    /**
     * 用户注册
     * @param registerVo
     */
    @Override
    public void register(RegisterVo registerVo) {
        //判断用户信息是否为空
        String nickname = registerVo.getNickname();
        String code = registerVo.getCode();
        String mobile = registerVo.getMobile();
        String password = registerVo.getPassword();
        if (StringUtils.isEmpty(nickname) ||
            StringUtils.isEmpty(code) ||
            StringUtils.isEmpty(mobile) ||
            StringUtils.isEmpty(password) ){
            throw new SelfException(20001,"注册信息不能为空");
        }

        //判断验证码是否相等
        String mobileCode = redisTemplate.opsForValue().get(mobile);
        if (!code.equals(mobileCode)){
            throw new SelfException(20001,"验证码错误");
        }
        
        //判断用户手机是否被注册过
        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile",mobile);
        int count = baseMapper.selectCount(wrapper);
        if (count>0){
            throw new SelfException(20001,"该手机号已经被注册");
        }

        //添加注册信息到数据库
        UcenterMember member = new UcenterMember();
        member.setNickname(nickname);
        member.setMobile(mobile);
        member.setPassword(MD5.encrypt(password));
        member.setIsDisabled(false);
        member.setAvatar("http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132");
        baseMapper.insert(member);
    }
}
