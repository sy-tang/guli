package com.atguigu.ucenterservice.mapper;

import com.atguigu.ucenterservice.entity.UcenterMember;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会员表 Mapper 接口
 * </p>
 *
 * @author 汤小白
 * @since 2020-07-10
 */
public interface UcenterMemberMapper extends BaseMapper<UcenterMember> {

}
