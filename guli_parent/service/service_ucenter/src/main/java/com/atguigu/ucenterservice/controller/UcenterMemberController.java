package com.atguigu.ucenterservice.controller;


import com.atguigu.ucenterservice.entity.UcenterMember;
import com.atguigu.ucenterservice.entity.vo.RegisterVo;
import com.atguigu.ucenterservice.service.UcenterMemberService;
import com.atguigu.utils.Result;
import com.atguigu.utils.jwt.JwtUtils;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 会员表 前端控制器
 * </p>
 *
 * @author 汤小白
 * @since 2020-07-10
 */
@RestController
@RequestMapping("/ucenterservice/ucenter")
@CrossOrigin
@Api(description = "注册登录")
public class UcenterMemberController {
    @Autowired
    UcenterMemberService ucenterMemberService;

    /**
     * 用户登录，用户数据生成token返回给前端
     * @param ucenterMember
     * @return
     */
    @PostMapping("login")
    public Result login(@RequestBody UcenterMember ucenterMember){
       String token = ucenterMemberService.login(ucenterMember);
       return Result.ok().data("token",token);
    }

    /**
     * 用户注册
     * @param registerVo
     * @return
     */
    @PostMapping("register")
    public Result register(@RequestBody RegisterVo registerVo){
        ucenterMemberService.register(registerVo);
        return  Result.ok();
    }

    /**
     * 从token中获取用户信息。
     * @param request
     * @return
     */
    @GetMapping("getMemberInfo")
    public Result getUcenterInfo(HttpServletRequest request){
        //从token中获取用户id
        String memberId = JwtUtils.getMemberIdByJwtToken(request);
        //通过用户id查询用户数据
        UcenterMember member = ucenterMemberService.getById(memberId);
        return Result.ok().data("item",member);
    }

}

