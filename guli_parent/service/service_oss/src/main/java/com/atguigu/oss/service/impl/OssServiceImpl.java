package com.atguigu.oss.service.impl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.atguigu.oss.service.OssService;
import com.atguigu.oss.utils.ConstPropertyUtils;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

/**
 * @author 汤小白
 * @date 2020/7/3
 */
@Service
public class OssServiceImpl implements OssService {
    @Override
    public String uploadFile(MultipartFile file) {

        String endpoint = ConstPropertyUtils.END_POINT;
        String accessKeyId = ConstPropertyUtils.KEY_ID;
        String accessKeySecret = ConstPropertyUtils.KEY_SECRET;
        String bucketName = ConstPropertyUtils.BUCKET_NAME;

        try {
            // 创建OSSClient实例。
            OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
            InputStream inputStream = file.getInputStream();  // 获取输入流
            String filename = file.getOriginalFilename();  // 获取文件名
            //给文件名添加唯一标识
            String uuid = UUID.randomUUID().toString().replaceAll("-","");
            filename=uuid+filename;
            //按日期存储文件
            String date = new DateTime().toString("yyyy/MM/dd");
            filename=date+filename;
            ossClient.putObject(bucketName, filename, inputStream);  // 保存文件到oss
            ossClient.shutdown();   // 关闭OSSClient。
            //https://edu-guli-bucket.oss-cn-beijing.aliyuncs.com/780.jfif
            String url ="https://"+bucketName+"."+endpoint+"/"+filename;  // 拼接保存路径
            return url;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }
}
