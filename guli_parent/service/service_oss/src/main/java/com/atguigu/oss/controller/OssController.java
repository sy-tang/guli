package com.atguigu.oss.controller;

import com.atguigu.oss.service.OssService;
import com.atguigu.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author 汤小白
 * @date 2020/7/3
 */
@RestController
@RequestMapping("/eduoss/fileoss")
@Api(description = "讲师头像上传")
@CrossOrigin
public class OssController {

    @Autowired
    OssService ossService;

    @ApiOperation(value = "上传头像")
    @PostMapping("upload")
    public Result uploadFile(
            @ApiParam(name = "file",value = "选择文件",required = true)
            MultipartFile file){

        String url = ossService.uploadFile(file);
        return Result.ok().data("url",url);
    }
}
