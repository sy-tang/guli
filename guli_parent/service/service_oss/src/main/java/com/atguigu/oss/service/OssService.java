package com.atguigu.oss.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author 汤小白
 * @date 2020/7/3
 */
public interface OssService {
    String uploadFile(MultipartFile file);
}
