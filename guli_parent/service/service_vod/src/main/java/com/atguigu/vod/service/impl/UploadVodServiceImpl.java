package com.atguigu.vod.service.impl;

import com.aliyun.vod.upload.impl.UploadVideoImpl;
import com.aliyun.vod.upload.req.UploadStreamRequest;
import com.aliyun.vod.upload.resp.UploadStreamResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.vod.model.v20170321.DeleteVideoRequest;
import com.aliyuncs.vod.model.v20170321.DeleteVideoResponse;
import com.atguigu.utils.Result;
import com.atguigu.utils.myexception.SelfException;
import com.atguigu.vod.service.UploadVodService;
import com.atguigu.vod.utils.ContsValue;
import com.atguigu.vod.utils.InitVodClient;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author 汤小白
 * @date 2020/7/7
 */
@Service
public class UploadVodServiceImpl implements UploadVodService {
    /**
     * 上传视频
     * @param file
     * @return 返回视频id
     */
    @Override
    public String uploadVod(MultipartFile file) {
        String fileName = file.getOriginalFilename();
        String title = fileName.substring(0,fileName.lastIndexOf("."));
        try {
            //上传
            String vodId = uploadStream(ContsValue.KEY_ID, ContsValue.KEY_SECRET, title, fileName, file.getInputStream());
            return vodId;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 批量删除视频
     * @param vodIds
     */
    @Override
    public void delBatch(List<String> vodIds) {
        try {
            DeleteVideoRequest request = new DeleteVideoRequest();
            //支持传入多个视频ID，多个用逗号分隔
            String ids = StringUtils.join(vodIds.toArray(), ","); //集合转字符串"1,2,3"
            request.setVideoIds(ids);
            DefaultAcsClient client = InitVodClient.initVodClient(ContsValue.KEY_ID, ContsValue.KEY_SECRET);
            client.getAcsResponse(request);
        } catch (Exception e) {
            e.printStackTrace();
            throw  new SelfException(20001,"批量删除视频失败");
        }
    }


    /**
     * 上传
     * @param accessKeyId
     * @param accessKeySecret
     * @param title
     * @param fileName
     * @param inputStream
     */
    private static String uploadStream(String accessKeyId, String accessKeySecret, String title, String fileName, InputStream inputStream) {
        //title  上传后显示的名称
        //fileName  视频名称
        UploadStreamRequest request = new UploadStreamRequest(accessKeyId, accessKeySecret, title, fileName, inputStream);
        UploadVideoImpl uploader = new UploadVideoImpl();
        UploadStreamResponse response = uploader.uploadStream(request);
        //System.out.print("RequestId=" + response.getRequestId() + "\n");  //请求视频点播服务的请求ID
        if (response.isSuccess()) {
            //System.out.print("VideoId=" + response.getVideoId() + "\n");
            return response.getVideoId();
        } else { //如果设置回调URL无效，不影响视频上传，可以返回VideoId同时会返回错误码。其他情况上传失败时，VideoId为空，此时需要根据返回错误码分析具体错误原因
            return null;
        }
    }
}
