package com.atguigu.vod.controller;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.vod.model.v20170321.DeleteVideoRequest;
import com.atguigu.utils.Result;
import com.atguigu.utils.myexception.SelfException;
import com.atguigu.vod.service.UploadVodService;
import com.atguigu.vod.utils.ContsValue;
import com.atguigu.vod.utils.InitVodClient;
import com.sun.org.apache.xpath.internal.objects.XString;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author 汤小白
 * @date 2020/7/7
 */
@RestController
@RequestMapping("/vod")
@CrossOrigin
@Api(description = "上传视频")
public class UploadVodController {
    @Autowired
    UploadVodService vodService;

    /**
     * 上传视频
     * @param file
     * @return
     */
    @ApiOperation("上传视频")
    @PostMapping("uploadVod")
    public Result  uploadVod(MultipartFile file){
        String vodId = vodService.uploadVod(file);
        return Result.ok().data("vodId",vodId);
    }

    /**
     * 删除视频
     * @param id
     * @return
     */
    @DeleteMapping("delVod/{id}")
    public Result delVod(@PathVariable  String id){
        try {
            DeleteVideoRequest request = new DeleteVideoRequest();
            //支持传入多个视频ID，多个用逗号分隔
            request.setVideoIds(id);
            DefaultAcsClient client = InitVodClient.initVodClient(ContsValue.KEY_ID, ContsValue.KEY_SECRET);
            client.getAcsResponse(request);
            return Result.ok();
        } catch (Exception e) {
            e.printStackTrace();
            throw  new SelfException(20001,"删除视频失败");
        }
    }

    /**
     * 批量删除视频
     */
    @DeleteMapping("/delVodBatch")
    public Result delVodBatch(@RequestParam List<String> vodIds){
        vodService.delBatch(vodIds);
        return Result.ok();
    }
}


