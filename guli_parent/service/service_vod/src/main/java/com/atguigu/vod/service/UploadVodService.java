package com.atguigu.vod.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author 汤小白
 * @date 2020/7/7
 */
public interface UploadVodService {
    String uploadVod(MultipartFile file);

    void delBatch(List<String> vodIds);
}
