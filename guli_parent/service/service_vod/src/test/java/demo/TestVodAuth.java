package demo;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.vod.model.v20170321.GetVideoInfoRequest;
import com.aliyuncs.vod.model.v20170321.GetVideoInfoResponse;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthRequest;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthResponse;

/**
 * @author 汤小白
 * @date 2020/7/7
 * 获取视频凭证
 */
public class TestVodAuth {
    public static GetVideoPlayAuthResponse getAuthREsponse(DefaultAcsClient client) throws ClientException {
        GetVideoPlayAuthRequest request = new GetVideoPlayAuthRequest ();
        request.setVideoId("ca616c29c7ee44faacde98cdc7d6946d");
        return client.getAcsResponse(request);
    }

    public static void main(String[] args) throws ClientException {
        DefaultAcsClient client = InitObject.initVodClient("LTAI4GD6LNjH9kAcv4spAtfV", "v9ITDN3pc9d7XciZCnHk1msXcIKb37");

        GetVideoPlayAuthResponse response = new GetVideoPlayAuthResponse();
        response = getAuthREsponse(client);

        String playAuth = response.getPlayAuth();
        System.out.println(playAuth);

    }
}
