package demo;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.vod.model.v20170321.GetPlayInfoRequest;
import com.aliyuncs.vod.model.v20170321.GetPlayInfoResponse;

import java.util.List;

/**
 * @author 汤小白
 * @date 2020/7/7
 * 测试获取视频地址
 */
public class TestVod {


    public static GetPlayInfoResponse getPlayInfo(DefaultAcsClient client) throws ClientException {
        GetPlayInfoRequest request = new GetPlayInfoRequest();
        request.setVideoId("ca616c29c7ee44faacde98cdc7d6946d");
        return client.getAcsResponse(request);
    }

    //调用
    public static void main(String[] args) throws ClientException {
        DefaultAcsClient client = InitObject.initVodClient("LTAI4GD6LNjH9kAcv4spAtfV", "v9ITDN3pc9d7XciZCnHk1msXcIKb37");

        GetPlayInfoResponse response = new GetPlayInfoResponse();

        response = getPlayInfo(client);
        List<GetPlayInfoResponse.PlayInfo> playInfoList = response.getPlayInfoList();
        for (GetPlayInfoResponse.PlayInfo playInfo: playInfoList) {
            String url = playInfo.getPlayURL();  //获取视频地址
            System.out.println(url);
        }

    }
}
