package com.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author 汤小白
 * @date 2020/8/3
 */
@SpringBootApplication
@EnableDiscoveryClient
public class ProdectApplication {
    public static void main(String[] args) {
        SpringApplication.run(ProdectApplication.class,args);
    }
}
