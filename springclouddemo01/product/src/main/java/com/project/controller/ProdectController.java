package com.project.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 汤小白
 * @date 2020/8/3
 */
@RestController
@RequestMapping("/product")
public class ProdectController {
    @Value("${server.port}")
    String port;

    @GetMapping("/lb")
    public String port(){
        return port;
    }
}
