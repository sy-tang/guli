package com.project.uaa.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;

/**
 * @author 汤小白
 * @date 2020/7/16
 */
@Configuration
public class TokenConfig {

    /**
     * 令牌存储策略
     * @return
     */
    @Bean
    public TokenStore tokenStore(){
        return new InMemoryTokenStore();  //暂时配置内存存储的令牌信息
    }
}
