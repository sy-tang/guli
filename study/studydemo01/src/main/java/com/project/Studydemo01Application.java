package com.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Studydemo01Application {

    public static void main(String[] args) {
        SpringApplication.run(Studydemo01Application.class, args);
    }

}
