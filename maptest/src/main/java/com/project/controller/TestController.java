package com.project.controller;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author 汤小白
 * @date 2020/8/2
 */
@RestController
@Api(tags = "测试")
public class TestController {

    @PostMapping("/test")
    public String test(@RequestParam Map<String,Object> map){
         String page = (String) map.get("page");
        String limit = (String) map.get("limit");
        System.out.println(page);
        System.out.println(limit);
        return page;
    }

}

