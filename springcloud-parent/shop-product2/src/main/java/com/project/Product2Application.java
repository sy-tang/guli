package com.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author 汤小白
 * @date 2020/7/29
 */
@SpringBootApplication
@EnableEurekaClient
public class Product2Application {
    public static void main(String[] args) {
        SpringApplication.run(Product2Application.class,args);
    }
}
