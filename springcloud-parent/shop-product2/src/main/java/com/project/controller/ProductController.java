package com.project.controller;

import com.project.entity.Result;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * @author 汤小白
 * @date 2020/7/27
 */
@RestController
public class ProductController {
    @Value("${server.port}")
    String port;

    @RequestMapping("/getResult")
    public Result result() throws InterruptedException {
        //TimeUnit.SECONDS.sleep(2);
        Result result = new Result();
        result.setMessage("调用的是http://127.0.0.1:"+port+"/getResult");
        result.setPort(port);
        result.setUrl("getResult");
        return result;
    }
}
