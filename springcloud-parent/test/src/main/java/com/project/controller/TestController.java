package com.project.controller;

import com.project.entity.Person;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 汤小白
 * @date 2020/7/28
 */
@RestController
public class TestController {

    @GetMapping("/hello")
    public Person hello(){
        Person person = new Person();
        person.setAge(23);
        person.setName("tang");
        return person;
    }
}
