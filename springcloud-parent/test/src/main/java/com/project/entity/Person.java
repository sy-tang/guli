package com.project.entity;

import lombok.Data;

/**
 * @author 汤小白
 * @date 2020/7/28
 */
@Data
public class Person {
    private String name;
    private Integer age;
}
