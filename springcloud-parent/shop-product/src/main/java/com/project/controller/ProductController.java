package com.project.controller;

import com.project.entity.Result;
import com.project.service.impl.ProductServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 汤小白
 * @date 2020/7/27
 */
@RestController
public class ProductController {
    @Value("${server.port}")
    String port;


    @Autowired
    private ProductServiceImpl productService;

    @RequestMapping("/getResult")
    public Result result() throws InterruptedException {

        Result result = new Result();
        result.setMessage(productService.message());
        result.setPort(port);
        result.setUrl("getResult");
        return result;
    }

    @GetMapping("/hello")
    public String say(){
        return "say hello";
    }

    @GetMapping("/test/{id}")
    public String test(@PathVariable("id") int id){
        String test = productService.test(id);
        return test;
    }

    @GetMapping("/wu")
    public String wu(){
        return "服务被断路后，其他方法能够正常调用吗？";
    }
}
