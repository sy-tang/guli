package com.project;

import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author 汤小白
 * @date 2020/7/28
 */
@SpringBootApplication
@EnableEurekaClient
@EnableCircuitBreaker  // 开启服务降级
public class ProductApplication {
    public static void main(String[] args) {
        SpringApplication.run(ProductApplication.class,args);
    }
}
