package com.project.entity;

import lombok.Data;

/**
 * @author 汤小白
 * @date 2020/7/27
 */
@Data
public class Result {
    private String url;
    private String port;
    private String message;

}
