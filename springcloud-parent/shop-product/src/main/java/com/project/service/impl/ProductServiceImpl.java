package com.project.service.impl;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.project.service.ProductService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;

import java.sql.Time;
import java.util.concurrent.TimeUnit;

/**
 * @author 汤小白
 * @date 2020/8/1
 */
@Service
public class ProductServiceImpl implements ProductService {

    // 使用服务降级  3秒之内执行完是正常的，超过3秒或者方法执行异常时，会到回退方法中去
    // 在服务方不太适合使用服务降级，因为需要将回退方法写在业务代码中
    @HystrixCommand(fallbackMethod = "bachMessage",commandProperties = {
            @HystrixProperty(name="execution.isolation.thread.timeoutInMilliseconds",value = "3000")
    })
    @Override
    public String message() throws InterruptedException {
        //int i = 10 / 0;
        int time = 2;
        //TimeUnit.SECONDS.sleep(time);
        String message= Thread.currentThread().getName()+ "：执行了"+time+"秒";
        return message;
    }

    // 这是回退方法
    public String bachMessage() {
        return Thread.currentThread().getName()+": 该服务繁忙，请稍后再试";
    }

    //========================服务熔断=============================
    @HystrixCommand(fallbackMethod = "fallTest",commandProperties = {
            @HystrixProperty(name = "circuitBreaker.enabled",value = "true"),  // 启动熔断器
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold",value = "10"),  // 请求次数
            @HystrixProperty(name="circuitBreaker.sleepWindowInMilliseconds", value = "5000"), // 时间窗口期，5秒后进入半开状态
            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage" , value = "60") // 失败率
            //10s内请求次数10次中失败率达到0.6时，就熔断。
   })
    public String test(int id){
        if (id < 0){
            throw new RuntimeException("id不能小于0");
        }
        return "正常执行的情况"+id;
    }

    public String fallTest(int id){
        return "走的是fallback方法  id是:"+id;
    }
}
