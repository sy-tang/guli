package com.project.service;


/**
 * @author 汤小白
 * @date 2020/8/1
 */
public interface ProductService {
    public String message() throws InterruptedException;
}
