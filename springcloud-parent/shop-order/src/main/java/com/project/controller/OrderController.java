package com.project.controller;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.project.entity.Result;
import com.project.feign.ProductFegin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * @author 汤小白
 * @date 2020/7/27
 */
@RestController

public class OrderController {

    @Autowired
    RestTemplate restTemplate;
    @Autowired
    DiscoveryClient discoveryClient;  // 获取所有注册的服务信息[服务名、端口、ip地址等]
    @Autowired
    ProductFegin productFegin;

    @RequestMapping(value = "/get",method = RequestMethod.GET)
    @HystrixCommand(commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "2000")
    })
    public Result get(){
        //Result result = restTemplate.getForObject("http://shop-product/getResult", Result.class);
        Result result = productFegin.result();  // 使用feign调用
        //List<ServiceInstance> instances = discoveryClient.getInstances("shop-product");
        //for (ServiceInstance instance : instances){
        //    System.out.println(instance);
        //    System.out.println(instance.getPort());
        //}
        return result;
    }

    @GetMapping("/hello")
    public String hello(){
        return productFegin.say();
    }
}
