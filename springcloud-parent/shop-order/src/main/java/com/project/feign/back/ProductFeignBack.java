package com.project.feign.back;

import com.project.entity.Result;
import com.project.feign.ProductFegin;
import org.springframework.stereotype.Component;

/**
 * @author 汤小白
 * @date 2020/8/2
 */
@Component
public class ProductFeignBack implements ProductFegin {

    @Override
    public Result result() {
        Result result = new Result();
        result.setMessage(Thread.currentThread().getName()+":调用product繁忙，稍后再试");
        return result;
    }

    @Override
    public String say() {
        return "hello 说不成了，说拜拜把";
    }
}
