package com.project.feign;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.project.entity.Result;
import com.project.feign.back.ProductFeignBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author 汤小白
 * @date 2020/7/30
 */
@Component
@FeignClient(name = "shop-product", fallback = ProductFeignBack.class)
public interface ProductFegin {

    @RequestMapping("/getResult")
    public Result result();

    @GetMapping("/hello")
    public String say();
}
