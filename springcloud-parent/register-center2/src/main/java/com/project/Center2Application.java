package com.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author 汤小白
 * @date 2020/7/29
 */
@SpringBootApplication
@EnableEurekaServer
public class Center2Application {
    public static void main(String[] args) {
        SpringApplication.run(Center2Application.class,args);
    }
}
